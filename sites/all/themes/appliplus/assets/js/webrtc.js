function initializeSession(apiKey, sessionId, token, pubname) {

    var users = { };
    var session = OT.initSession(apiKey, sessionId);
    var subs = 0;
    var connectionCount = 0;


    // Subscribe to a newly created stream
    session.on('streamCreated', function (event) {

        console.log("Stream Created");
        console.log(event.stream);

        var name = event.stream.name;
        var name_to_id = nameToID(name);

        subs++;
        //changeAlert("An user just came online", 'alert-info');

        var actWid = $('.col-md-12 .panel-body').width();
        if (actWid > 900) {
            actWid = actWid / 2
        }

        var sub_width = actWid - 20;
        var sub_height = (9 * sub_width) / 16;

        var Sub = session.subscribe(event.stream, 'subscriber', {
            insertMode: 'append',
            width: sub_width,
            height: sub_height,
            showControls: true
        });

        Sub.setStyle({style: {buttonDisplayMode: 'on'}});


        var SubID = Sub.id;
        var connectionId = event.stream.connection.connectionId;
        console.log("Connection id: " + connectionId);
        users.connectionId.stream = event.stream;
        users.connectionId.OTid = SubID;

        if(users) {
            console.log("Users");
            console.log(users);
        }

    });

    session.on('sessionDisconnected', function (event) {
        showErrorModal('You were disconnected from the session.' + event.reason, "Attention");
    });

    // Connect to the session
    session.connect(token, function (error) {
        // If the connection is successful, initialize a publisher and publish to the session
        if (!error) {

            var pub_width = $('.col-md-4 .panel-body').width();
            var pub_height = (9 * pub_width) / 16;

            var publisher = OT.initPublisher('publisher', {name: pubname, mirror: false});
            session.publish(publisher);

            publisher.on('streamCreated', function (event) {
                changeAlert('You\'re ready! Please wait for other users arrive online!', 'alert-success');

                $('#publisher').width(pub_width).height(pub_height);
            });


        } else {
            //changeAlert('There was an error connecting to the session: ', error.code, error.message, 'alert-danger');
        }
    });

    session.on({
        connectionCreated: function (event) {
            connectionCount++;
            if (event.connection.connectionId != session.connection.connectionId) {
                //changeAlert('Another client connected. ' + connectionCount + ' total.', 'alert-danger');
                console.log("Connection Created");
                console.log(event.connection);
                users[event.connection.connectionId]["connection"] = event.connection;

            }
        },
        connectionDestroyed: function connectionDestroyedHandler(event) {
            connectionCount--;
            //changeAlert('A client disconnected. ' + connectionCount + ' total.', 'alert-info');
            console.log("Connection Disconnected");
            console.log(event.connection)
        }
    });

    function nameToID(name) {
        name = name.toLowerCase();
        name = name.replace(/[^A-Z0-9]+/ig, "_");
        return name;
    }

}
