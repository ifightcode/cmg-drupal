<script>
    var menu = {};
    var topMenu = {};
    <?php
    foreach(menu_navigation_links('main-menu') as $menu){
        echo "menu['".$menu['title']."'] = '".$menu['href']."';";
    }

    foreach(menu_navigation_links('user-menu') as $menu){
        echo "topMenu['".$menu['title']."'] = '".$menu['href']."';";
    }

    ?>
</script>
<ul class="float-menu" style="display: none"><!--<span class="float-menu-close">x</span>--></ul>
<nav class="navbar navbar-inverse topnvsm">
    <div class="container-fluid">
        <div class="col-md-10 col-md-offset-1">
            <form class="form-nav" role="search" action="/" method="post" accept-charset="UTF-8">
                <div class="form-group">
                    <input type="text" class="form-control search" placeholder="Search for people, group or media">
                    <input type="button" class="btnSearch" value="Search">
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right tnav">
                <li><span class="fa fa-search tsbtnt" style="display: none"></span></li>
                <li><span class="glyphicon glyphicon-user" aria-hidden="true"></span></li>
                <li><span class="glyphicon glyphicon-bell" aria-hidden="true"></span><span class="badge">19</span>
                </li>
                <li><span class="glyphicon glyphicon-comment" aria-hidden="true"></span><span
                        class="badge">19</span></li>

                <li class="dropdown">
                    <a href="#" role="button" aria-haspopup="true"
                       aria-expanded="false"><span class="fa fa-bars" aria-hidden="true"></span></a>
                </li>


            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav><!-- Top Navbar -->

<div style="display: none" class="thatSearch">
    <input type="text" placeholder="Search for people, group or media" class="tSearch">
    <span class="fa fa-search tsbtn"></span>
</div>

<ul class="float-menu-right" style="display: none"></ul>

<div class="helloAction"><!-- -->

    <div class="container-fluid mmain">

        <div class="col-md-10 col-md-offset-1 top-banner">
            <?php print render($page['banner']); ?>
            <nav class="customNav">
                <div class="main-menu" style="display: none;">Main Menu</div>
                <?php print render($page['main_menu']); ?>
            </nav>
        </div>

        <div class="col-md-10 col-md-offset-1 container-body">

            <!--
            <?php if ($breadcrumb): ?>
                <div id="breadcrumb"><?php print $breadcrumb; ?></div>
            <?php endif; ?>
            -->

            <?php if ($messages): ?>
                <div id="messages">
                    <div class="section clearfix">
                        <?php print $messages; ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="col-md-9"><?php  print render($page['content_sidebar']); ?></div>
            <div class="col-md-3"><?php  print render($page['sidebar']); ?></div>
            <?php print render($page['content']); ?>
        </div>

    </div><!-- container -->

    <?php print render($page['footer']); ?>
</div><!-- helloAction -->