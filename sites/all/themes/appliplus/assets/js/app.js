function ifRTCSupported() {
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
    window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;

    if (navigator.getUserMedia) {

        getMediaAccess();

    } else {
        showErrorModal("Your browser doesn't have WebRTC support. Please use latest version from one of the following<br /><br />" +
            "<ul>" +
            "<li><a href='https://www.google.com/chrome/'>Google Chrome</a></li>" +
            "<li><a href='http://www.mozilla.org/firefox'>Mozilla Firefox</a></li>" +
            "<li><a href='http://www.opera.com/download'>Opera</a></li>" +
            "</ul>" +
            "");
        window.stop();
    }

}

function getMediaAccess() {
    navigator.getUserMedia({video: true, audio: true}, successCallback, errorCallback);
    function errorCallback() {
        showErrorModal("Unfortunately we didn't get camera & mic access. You must allow both access to continue with us.<br>" +
            "Either your devices doesn't have Mic & Camera or those are disabled for security reason or you denied access.<br>" +
            "" +
            "");

        window.stop();
    }

    function successCallback() {
        console.log("We got media access")
    }
}

function showErrorModal(errMsg, errTitle) {
    var errorModal = $('.errorModal');
    $('.errorModalMsg').html(errMsg);
    $('.modal-title').html(errTitle);
    errorModal.modal();
}

function changeAlert(alertMsg, alertCls) {
    $('.alert').html(alertMsg).removeClass('alert-danger').removeClass('alert-info').removeClass('alert-success').addClass(alertCls);
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function myToggle(phead, pbody) {
    $(phead).click(function () {
        $(pbody).collapse('toggle');
    });
}

function curSysTime(){

    var today = new Date();

    var y = today.getFullYear();
    var M = today.getMonth() + 1;
    var d = today.getDate();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();

    M = checkTime(M);
    d = checkTime(d);
    m = checkTime(m);
    h = checkTime(h);
    s = checkTime(s);

    return y + "/" + M + "/" + d + " " + h + ":" + m + ":" + s;

}

function curESTtime(){
    return moment().tz("EST5EDT").format('YYYY/MM/DD HH:mm:ss');
}

function startTime() {
    $('.clock').text(curESTtime());
    var t = setTimeout(startTime, 500);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getBrowserHeight() {
    var myWidth = 0, myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }

    return myHeight;
}