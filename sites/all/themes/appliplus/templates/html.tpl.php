<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic' rel='stylesheet'
          type='text/css'>
    <link href="/<?php print $directory; ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <?php print $styles; ?>
    <script>
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }
    </script>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<?php print $page_top; ?>
<?php print $page; ?>
<!-- Bootstrap modal -->
<div class="modal fade errorModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p class="errorModalMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.3/moment-timezone-with-data.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script src="/<?php print $directory; ?>/assets/js/app.js"></script>
<script src="/<?php print $directory; ?>/assets/js/notify.js"></script>
<?php print $scripts; ?>
<script type="text/javascript">

    $(document).ready(function () {
        floatMenu = false;

        var height = getBrowserHeight() - 200;
        $('.container-body').css("min-height", height);

        $('.btn-subs').click(function (e) {
            e.preventDefault();
            var chkBox = $('.chk-accept');

            if (chkBox.is(':checked')) {
                var email = $('.subs-email');
                if (isEmail(email.val())) {
                    //All valid, now let's process the request

                } else {
                    alert("Please enter valid email");
                    email.focus();
                }

            } else {
                alert("Please accept you want to get updates");
                chkBox.focus();
            }
        });

        marginTopMenuR();
        //reHeightFloatMenuR();

        //Add items to top menu

        for (var t in topMenu) {
            var mnu = '<li><a href="' + topMenu[t] + '"> ' + t + ' </a></li>';
            $('.float-menu-right').append(mnu);
        }

        var searchOpen = false;
        $('.tsbtnt').click(function (e) {

            if (searchOpen) {
                searchOpen = false;
                $('.thatSearch').css("display", "none");
            } else {
                searchOpen = true;
                $('.thatSearch').css("display", "block");
            }
        });

        var TopOneOpened = false;
        $('.fa-bars').click(function (e) {
            var winWdt = $(window).width();

            if (TopOneOpened) {
                TopOneOpened = false;
                if (winWdt > 900) {
                    positionTopRightMenuFromLeft();
                }
                $('.float-menu-right').css("display", "none");
            } else {
                TopOneOpened = true;
                if (winWdt > 900) {
                    positionTopRightMenuFromLeft();
                }
                $('.float-menu-right').css("display", "block");
            }
        });

        $(window).resize(function () {
            marginTopMenuR();
            //reHeightFloatMenuR();
            positionTopRightMenuFromLeft();
        });

        $('.logout').click(function (e) {
            e.preventDefault();

            $.ajax({
                url: "/login/logout",
                success: function (data) {
                    alert(data);
                    window.location = "/"
                }
            });
        });

        homepage_script();

        $.each( menu, function( index, value ){
            $('.float-menu').append('<li><a href="'+value+'">'+index+'</a></li>');
        });

        $('.float-menu-close').click(function (e) {
            console.log("Clicked");
            $('.float-menu').css("display", "none");
        });

        $('.main-menu').click(function (e) {

            if (floatMenu) {
                floatMenu = false;
                $('.float-menu').css("display", "none");
                $('.helloAction').css("position", "").css("left", "").css("right", "")
            } else {
                floatMenu = true;
                $('.float-menu').css("display", "block");
                //$('.helloAction').css("position", "absolute").css("left", $('.float-menu').width()).css("right", -1*$('.float-menu').width())
            }
        });

        marginTopMenu();
        //reHeightFloatMenu();

        $(window).resize(function () {
            //marginTopMenu();
            //reHeightFloatMenu();
        });

        $($('.errorMessage')[0]).css('display', 'block');


    });

    function positionTopRightMenuFromLeft() {
        var fmr = $('.float-menu-right');
        var cb = $('.container-body');
        var lvn = cb.offset().left + cb.width();
        fmr.css("left", lvn - fmr.width());
    }
    function marginTopMenuR() {
        var topbar = $('.toolbar-menu').height();
        if(topbar < 10){
            topbar = 0;
        }

        var topnvsm = $('.topnvsm').height() + topbar;
        console.log("Top Nav (Notif area) height: ", topnvsm);
        $('.float-menu-right').css("margin-top", topnvsm);
    }
    function reHeightFloatMenuR() {
        var windowHeight = $(window).height();
        console.log("Current window height: ", windowHeight);
        $('.float-menu-right').css("height", windowHeight);
    }
    function homepage_script() {

        $('.carousel').carousel();

        //Open all
        $('.iwtsb').collapse('show');
        $('.ntb').collapse('show');
        $('.dsbb').collapse('show');
        $('.frb').collapse('show');
        $('.ub').collapse('show');
        $('.wob').collapse('show');
        $('.frmb').collapse('show');
        $('.grpb').collapse('show');
        $('.polb').collapse('show');
        $('.vidb').collapse('show');
        $('.calb').collapse('show');
        $('.tmb').collapse('show');

        //Toggler
        myToggle('.iwtsh', '.iwtsb');
        myToggle('.nth', '.ntb');
        myToggle('.dsbh', '.dsbb');
        myToggle('.frh', '.frb');
        myToggle('.uh', '.ub');
        myToggle('.woh', '.wob');
        myToggle('.frmh', '.frmb');
        myToggle('.grph', '.grpb');
        myToggle('.polh', '.polb');
        myToggle('.vidh', '.vidb');
        myToggle('.calh', '.calb');
        myToggle('.tmh', '.tmb');
    }
    function marginTopMenu() {
        var topbar = $('.toolbar-menu').height();
        if(topbar < 10){
            topbar = 0;
        } else {
            topbar = topbar / 2;
        }
        //var topnvsm = $('.topnvsm').height();
        //console.log("Top Nav (Notif area) height: ", topnvsm);
        $('.float-menu').css("top", $('.customNav').offset().top + topbar + 40)
        //$('.float-menu').css("margin-top", topnvsm);
    }
    function reHeightFloatMenu() {
        var windowHeight = $(window).height();
        console.log("Current window height: ", windowHeight);
        $('.float-menu').css("height", windowHeight);
    }

</script>
<?php print $page_bottom ?>
<?php
//var_dump($user);
/*
 * object(stdClass)#6 (23) { ["uid"]=> string(1) "1" ["name"]=> string(8) "sktanmoy" ["pass"]=> string(55) "$S$DYoRaSy9iyvoF3/DHAlp/GhtD8WhPyUn4Zhkljq5KNkajHvX/VD6" ["mail"]=> string(18) "sktanmoy@gmail.com" ["theme"]=> string(0) "" ["signature"]=> string(0) "" ["signature_format"]=> NULL ["created"]=> string(10) "1470463199" ["access"]=> string(10) "1470500130" ["login"]=> string(10) "1470499887" ["status"]=> string(1) "1" ["timezone"]=> string(14) "Africa/Abidjan" ["language"]=> string(0) "" ["picture"]=> string(1) "0" ["init"]=> string(18) "sktanmoy@gmail.com" ["data"]=> bool(false) ["sid"]=> string(43) "TFx-jQ56_FC9IWtolnT_4iog9jLwvjMsyuh6fys6Uog" ["ssid"]=> string(0) "" ["hostname"]=> string(15) "162.158.166.161" ["timestamp"]=> string(10) "1470500166" ["cache"]=> string(1) "0" ["session"]=> string(0) "" ["roles"]=> array(2) { [2]=> string(18) "authenticated user" [3]=> string(13) "administrator" } }
 */
?>
</body>
</html>
