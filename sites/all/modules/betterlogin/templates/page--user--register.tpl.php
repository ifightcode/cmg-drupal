<div class="container">

    <div class="login">
        <h1><?php print $title; ?></h1>
        <?php print $messages; ?>
        <?php print render($page['content']); ?>
    </div>

    <div class="login-help">
        <div class="login_link">
            <?php print l(t('Login'), 'user/login'); ?>
        </div>

        <div class="password_link">
            <?php print l(t('Forgot your password?'), 'user/password'); ?>
        </div>
    </div>

</div>