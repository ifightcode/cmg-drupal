<div class="container">

    <div class="login">
        <h1><?php print $title; ?></h1>
        <?php print $messages; ?>
        <?php print render($page['content']); ?>
    </div>

    <div class="login-help">
        <div class="login_link">
            <?php print l(t('Login'), 'user/login'); ?>
        </div>

        <?php if (variable_get('user_register')): ?>
            <div class="register_link">
                <?php print l(t('Register a new account'), 'user/register'); ?>
            </div>
        <?php endif; ?>
    </div>

</div>